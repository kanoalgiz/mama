package com.mama.controller.rest;

import com.mama.converter.CrewConverter;
import com.mama.domain.Crew;
import com.mama.domain.Gangster;
import com.mama.dto.CrewForm;
import com.mama.dto.CrewView;
import com.mama.service.CrewService;
import com.mama.service.GangsterService;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/crew")
@RequiredArgsConstructor
public class CrewRestController {

  private final CrewService crewService;
  private final CrewConverter crewConverter;
  private final GangsterService gangsterService;

  @GetMapping("/{id}")
  public CrewView getCrew(@PathVariable("id") Long id) {

    Crew crew = crewService.getById(id);

    return crewConverter.convertToView(crew);
  }

  @GetMapping("/all")
  public List<CrewView> getCrews() {

    List<Crew> crews = crewService.getAll();

    return crews.stream()
        .map(crewConverter::convertToView)
        .collect(Collectors.toList());
  }

  @PostMapping
  public Long saveCrew(@RequestBody @Valid CrewForm crewForm) {

    Crew crew = crewConverter.convertToEntity(crewForm);
    Crew newCrew = crewService.save(crew);

    return newCrew.getId();
  }

  @PutMapping("/{id}")
  public void updateCrew(@PathVariable("id") Long id, @RequestBody @Valid CrewForm crewForm) {

    crewService.update(id, crewForm);
  }

  @DeleteMapping("/{id}")
  public void deleteCrew(@PathVariable("id") Long id) {

    Crew crew = crewService.getById(id);
    crew.getGangsters().forEach(gangster -> gangster.setCrew(null));
    crewService.delete(id);
  }

  @PutMapping("/{crewId}/add-member/{gangsterId}")
  public void addGangster(@PathVariable("crewId") Long crewId, @PathVariable("gangsterId") @Valid Long gangsterId) {

    Crew crew = crewService.getById(crewId);
    Gangster gangster = gangsterService.getById(gangsterId);
    gangster.setCrew(crew);
    gangsterService.save(gangster);
  }

  @PutMapping("/{crewId}/remove-member/{gangsterId}")
  public void removeGangster(@PathVariable("crewId") Long crewId, @PathVariable("gangsterId") @Valid Long gangsterId) {

    Gangster gangster = gangsterService.getById(gangsterId);
    gangster.setCrew(null);
    gangsterService.save(gangster);
  }
}
