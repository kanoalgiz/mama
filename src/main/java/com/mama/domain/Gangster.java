package com.mama.domain;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Gangster {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column
  private String fullName;

  @Enumerated(EnumType.STRING)
  private Role role;

  @Column
  private String comment;

  @ManyToOne
  @JoinColumn(name = "crew_id", foreignKey = @ForeignKey(name = "crew_id_fk"))
  private Crew crew;

  @ManyToMany(cascade = CascadeType.MERGE)
  @JoinTable(name = "gangster_skill", inverseJoinColumns = @JoinColumn(name = "skill_id", referencedColumnName = "id"))
  private List<Skill> skills;
}
