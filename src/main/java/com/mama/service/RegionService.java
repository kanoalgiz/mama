package com.mama.service;

import com.mama.domain.Region;
import com.mama.dto.RegionForm;
import java.util.List;

public interface RegionService {

  Region getOrCreate(String name);

  Region save(Region region);

  Region getById(Long id);

  void update(Long id, RegionForm regionForm);

  List<Region> getAll();
}
