package com.mama.service;

import com.mama.domain.Gangster;
import com.mama.domain.Skill;
import com.mama.dto.GangsterForm;
import com.mama.repository.GangsterRepository;
import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class GangsterServiceImpl implements GangsterService {

  public static final String GANGSTER_NOT_FOUND = "Gangster not found";

  private final GangsterRepository gangsterRepository;
  private final SkillService skillService;

  @Override
  @Transactional
  public Gangster getById(Long id) {

    return gangsterRepository.findById(id)
        .orElseThrow(() -> new IllegalArgumentException(GANGSTER_NOT_FOUND));
  }

  @Override
  @Transactional
  public Gangster save(Gangster gangster) {

    if (gangster == null) {
      throw new IllegalArgumentException("Gangster is null");
    }

    return gangsterRepository.save(gangster);
  }

  @Override
  @Transactional
  public void delete(Long id) {

    boolean exists = gangsterRepository.existsById(id);

    if (!exists) {
      throw new IllegalArgumentException(GANGSTER_NOT_FOUND);
    }

    gangsterRepository.deleteById(id);
  }

  @Override
  @Transactional
  public List<Gangster> getAll() {

    return gangsterRepository.findAll();
  }

  @Override
  @Transactional
  public void update(Long id, GangsterForm gangsterForm) {

    Gangster gangster = getById(id);

    String comment = gangsterForm.getComment() == null ? "" : gangsterForm.getComment();
    gangster.setComment(comment);
    gangster.setFullName(gangsterForm.getFullName());
    gangster.setRole(gangsterForm.getRole());

    List<Skill> skills = gangsterForm.getSkillsId()
        .stream()
        .map(skillService::getById)
        .collect(Collectors.toList());

    gangster.setSkills(skills);
    save(gangster);
  }
}
