package com.mama.service;

import com.mama.domain.Crew;
import com.mama.domain.Region;
import com.mama.dto.CrewForm;
import com.mama.repository.CrewRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CrewServiceImpl implements CrewService {

  public static final String CREW_NOT_FOUND = "Crew not found";

  private final CrewRepository crewRepository;
  private final RegionService regionService;

  @Override
  @Transactional
  public Crew getById(Long id) {

    return crewRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(CREW_NOT_FOUND));
  }

  @Override
  @Transactional
  public void delete(Long id) {

    Crew crew = crewRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(CREW_NOT_FOUND));
    crewRepository.delete(crew);
  }

  @Override
  @Transactional
  public Crew save(Crew crew) {

    if (crew == null) {
      throw new IllegalArgumentException("Crew is null");
    }

    return crewRepository.save(crew);
  }

  @Override
  @Transactional
  public List<Crew> getAll() {

    return crewRepository.findAll();
  }

  @Override
  @Transactional
  public void update(Long id, CrewForm crewForm) {

    Crew crew = crewRepository.findById(id).orElseThrow(() -> new IllegalArgumentException(CREW_NOT_FOUND));
    String comment = crewForm.getComment() == null ? "" : crewForm.getComment();
    crew.setComment(comment);
    crew.setName(crewForm.getName());
    crew.setOccupation(crewForm.getOccupation());
    Region region = regionService.getById(crewForm.getRegionId());
    crew.setRegion(region);
    save(crew);
  }
}
