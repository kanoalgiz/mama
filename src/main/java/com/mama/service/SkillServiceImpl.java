package com.mama.service;

import com.mama.domain.Skill;
import com.mama.dto.SkillForm;
import com.mama.repository.SkillRepository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class SkillServiceImpl implements SkillService {

  private final SkillRepository skillRepository;

  @Override
  @Transactional
  public Skill getOrCreate(String name) {

    Skill skill = skillRepository.getByName(name);

    if (skill == null) {
      Skill newSkill = Skill.builder()
          .name(name)
          .build();
      skill = skillRepository.save(newSkill);
    }

    return skill;
  }

  @Override
  public Skill save(Skill skill) {

    if (skill == null) {
      throw new IllegalArgumentException("Region is null");
    }

    return skillRepository.save(skill);
  }

  @Override
  public Skill getById(Long id) {

    return skillRepository.findById(id)
        .orElseThrow(() -> new IllegalArgumentException("Skill not found"));
  }

  @Override
  public void update(Long id, SkillForm skillForm) {

    Skill skill = getById(id);
    skill.setName(skillForm.getName());
    save(skill);
  }

  @Override
  public List<Skill> getAll() {

    return skillRepository.findAll();
  }
}
