package com.mama.service;

import com.mama.domain.Gangster;
import com.mama.dto.GangsterForm;
import java.util.List;

public interface GangsterService {

  List<Gangster> getAll();

  void delete(Long id);

  Gangster save(Gangster gangster);

  Gangster getById(Long id);

  void update(Long id, GangsterForm gangsterForm);
}
