package com.mama.converter;

import com.mama.domain.Crew;
import com.mama.domain.Gangster;
import com.mama.domain.Region;
import com.mama.dto.CrewForm;
import com.mama.dto.CrewView;
import com.mama.dto.GangsterView;
import com.mama.dto.RegionView;
import com.mama.service.RegionService;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CrewConverter {

  private final RegionConverter regionConverter;
  private final GangsterConverter gangsterConverter;
  private final RegionService regionService;

  public Crew convertToEntity(CrewForm crewForm) {

    Region region = regionService.getById(crewForm.getRegionId());
    String comment = crewForm.getComment() == null ? "" : crewForm.getComment();

    return Crew.builder()
        .name(crewForm.getName())
        .occupation(crewForm.getOccupation())
        .comment(comment)
        .region(region)
        .build();
  }

  public CrewView convertToView(Crew crew) {

    List<GangsterView> gangsterViews = new ArrayList<>();
    List<Gangster> gangsters = crew.getGangsters();

    for (Gangster gangster : gangsters) {
      GangsterView gangsterView = gangsterConverter.convertToView(gangster);
      gangsterViews.add(gangsterView);
    }

    RegionView regionView = regionConverter.convertToView(crew.getRegion());

    return CrewView.builder()
        .id(crew.getId())
        .name(crew.getName())
        .occupation(crew.getOccupation())
        .comment(crew.getComment())
        .region(regionView)
        .gangsters(gangsterViews)
        .build();
  }
}
