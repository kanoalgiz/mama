package com.mama.converter;

import com.mama.domain.Skill;
import com.mama.dto.SkillForm;
import com.mama.dto.SkillView;
import org.springframework.stereotype.Component;

@Component
public class SkillConverter {

  public Skill convertToEntity(SkillForm skillForm) {

    return Skill.builder()
        .name(skillForm.getName())
        .build();
  }

  public SkillView convertToView(Skill skill) {

    return SkillView.builder()
        .id(skill.getId())
        .name(skill.getName())
        .build();
  }
}
