package com.mama.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

import com.mama.domain.Crew;
import com.mama.service.CrewService;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;

@ExtendWith(MockitoExtension.class)
class CrewControllerTest {

  @Captor
  ArgumentCaptor<List<Crew>> argumentCaptor;

  @Mock
  Model model;

  @Mock
  CrewService crewService;

  @InjectMocks
  CrewController crewController;

  @Test
  void getCrews_htmlExists_stringReturned() {

    // GIVEN
    Crew crew = new Crew();
    Mockito.when(crewService.getAll()).thenReturn(List.of(crew));

    // WHEN
    String result = crewController.getCrews(model);

    // THEN
    assertEquals("crews.html", result);

    Mockito.verify(model).addAttribute(eq("crews"), argumentCaptor.capture());
    List<Crew> crews = argumentCaptor.getValue();

    assertEquals(1, crews.size());
    assertEquals(crew, crews.get(0));
  }
}
